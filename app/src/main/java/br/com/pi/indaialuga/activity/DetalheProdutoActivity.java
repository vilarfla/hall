package br.com.pi.indaialuga.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import br.com.pi.indaialuga.R;
import br.com.pi.indaialuga.model.Anuncio;

public class DetalheProdutoActivity extends AppCompatActivity {

    private CarouselView carouselView;
    private TextView titulo;
    private TextView preco;
    private TextView bairro;
    private TextView descricao;
    private Anuncio anuncioSelecionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_detalhe_produto);

        //Configura ToolBar
        getSupportActionBar().setTitle("Detalhe Imóvel");

        //Inicializa componentes de interface
        inicializaComponentes();

        //Recupera anúncio para exibicao
        anuncioSelecionado = (Anuncio) getIntent().getSerializableExtra("anuncioSelecionado");

        if(anuncioSelecionado != null){

            titulo.setText(anuncioSelecionado.getTitulo());
            descricao.setText(anuncioSelecionado.getDescricao());
            bairro.setText(anuncioSelecionado.getBairro());
            preco.setText(anuncioSelecionado.getValor());


            ImageListener imageListener = new ImageListener() {
                @Override
                public void setImageForPosition(int position, ImageView imageView) {

                    String urlString = anuncioSelecionado.getFotos().get(position);
                    Picasso.get().load(urlString).into(imageView);

                }
            };

            carouselView.setPageCount(anuncioSelecionado.getFotos().size());
            carouselView.setImageListener(imageListener );

        }
        else{

        }
    }

    public void visualizarTelefone(View view){

        Intent i = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", anuncioSelecionado.getTelefone(),null));
        startActivity(i);

    }


    private void inicializaComponentes(){
        carouselView = findViewById(R.id.carouselView);
        titulo = findViewById(R.id.txtTituloDetalhe);
        descricao = findViewById(R.id.txtDescricaoDetalhe);
        bairro = findViewById(R.id.txtBairroDetalhe);
        preco = findViewById(R.id.txtPrecoDetalhe);
    }

}
