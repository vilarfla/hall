package br.com.pi.indaialuga.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import br.com.pi.indaialuga.R;

public class iotActivity extends AppCompatActivity {

    //Variáveis
    static String MQTTHOST = "tcp://soldier.cloudmqtt.com:16289"; //apontamento mqtt cloud
    static String USERNAME = "qmsckjxw";
    static String PASSWORD = "7FhpnddzzyqC";
    String topicStr = "LED"; //Topico criado
    MqttAndroidClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iot);

        //Verificação da conexão com o mqtt cloud

        String clientId = MqttClient.generateClientId();
        client = new MqttAndroidClient(this.getApplicationContext(), MQTTHOST, clientId);
        MqttConnectOptions options = new MqttConnectOptions();
        options.setUserName(USERNAME);
        options.setPassword(PASSWORD.toCharArray());

        try {
            IMqttToken token = client.connect(options);
            token.setActionCallback(new IMqttActionListener() {

                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Toast.makeText(iotActivity.this,"conectado",Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Toast.makeText(iotActivity.this,"não conectado",Toast.LENGTH_LONG).show();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }

    }

//Métodos para os botões


    public void abrir(View v){
        String topic = topicStr;
        String message = "A1";
        try {
            client.publish(topic, message.getBytes(),0,false    );
            Toast.makeText(iotActivity.this,"Porta Aberta",Toast.LENGTH_LONG).show();
        } catch (MqttException e) {
            e.printStackTrace();
        }


    }

    public void fechar(View v){
        String topic = topicStr;
        String message = "F1";
        try {
            client.publish(topic, message.getBytes(),0,false    );
            Toast.makeText(iotActivity.this,"Porta Fechada",Toast.LENGTH_LONG).show();
        } catch (MqttException e) {
            e.printStackTrace();
        }


    }





    public void ligar(View v){
        String topic = topicStr;
        String message = "L1";
        try {
            client.publish(topic, message.getBytes(),0,false    );
            Toast.makeText(iotActivity.this,"Luzes acesas",Toast.LENGTH_LONG).show();
        } catch (MqttException e) {
            e.printStackTrace();
        }


    }


    public void desligar(View v){
        String topic = topicStr;
        String message = "D1";
        try {
            client.publish(topic, message.getBytes(),0,false    );
            Toast.makeText(iotActivity.this,"Luzes apagadas",Toast.LENGTH_LONG).show();
        } catch (MqttException e) {
            e.printStackTrace();
        }


    }

}
